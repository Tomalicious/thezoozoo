package com.realdolmen.services;

import com.realdolmen.domain.Food;
import com.realdolmen.domain.Tiger;
import com.realdolmen.repositories.FoodRepository;

import java.util.List;

public class FoodService {

    FoodRepository foodRepository = new FoodRepository();


    public void changeFoodName(String newName , int id){
        foodRepository.changeById(id , newName);
    }

    public void deleteFood(int id){
        foodRepository.deleteById(id);
    }

    public List<Tiger> findAllAnimalsByFoodsId(int id){
        return foodRepository.findAllFoodByAnimalId(id);
    }

}

