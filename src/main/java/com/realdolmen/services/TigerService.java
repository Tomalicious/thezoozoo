package com.realdolmen.services;

import com.realdolmen.domain.Tiger;
import com.realdolmen.repositories.FoodRepository;
import com.realdolmen.repositories.TigerRepository;

import java.sql.SQLException;
import java.util.List;

//Services can act as a Passtrough class and can have some business logic, but it's not necessary
//Anatomy of a Class: AccessModifier class ClassName
public class TigerService {

    FoodRepository foodRepository = new FoodRepository();
    private TigerRepository tigerRepository = new TigerRepository(); //this is an instance

    public List<Tiger> getTigers(){
        return tigerRepository.getTigersFromDb(); //return fieldName.methodName();
    }


    public void addATiger(Tiger tiger) { //tiger is the tiger that was created in "addNewTiger" method in MyApplication
        tigerRepository.addATigerInDb(tiger); // tiger is added to DB, see TigerRepository
    }
}




//TODO: fill in the Food list field of each Tiger in the list
// HINT: use a for each and call the method findFoodsByAnimalId in FoodRepository to get a food list specific for1 animal
