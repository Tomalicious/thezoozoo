package com.realdolmen.domain;

public class Food {

    private String animalFood;
   private int foodId;

    public Food(String animalFood, int foodId) {
        this.animalFood = animalFood;
        this.foodId = foodId;
    }

    public String getAnimalFood() {
        return animalFood;
    }

    public int getFoodId() {
        return foodId;
    }

    @Override
    public String toString() {
        return "Food{" +
                "animalFood='" + animalFood + '\'' +
                ", foodId=" + foodId +
                '}';
    }
}
