package com.realdolmen.repositories;

import com.realdolmen.domain.Food;
import com.realdolmen.domain.Tiger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FoodRepository {

    public void deleteById (int foodId) {
        String url = "jdbc:mysql://localhost:3306/zoo";
        Connection myConnection = null;
        try {
            myConnection = DriverManager.getConnection(url, "root", "P@ssw0rd");
            //INSERT INTO table_name (column1, column2, column3, ...)
            //VALUES (value1, value2, value3, ...);
            PreparedStatement myStatement = myConnection.prepareStatement("Delete from Food where id = ?");
            myStatement.setInt(1,foodId);
            myStatement.execute();

        } catch (SQLException e) {
            while (e != null) {
                System.out.println(e);
                e = e.getNextException();
            }
        }
    }
    public void changeById (int id , String foodName) {
        String url = "jdbc:mysql://localhost:3306/zoo";
        Connection myConnection = null;
        try {
            myConnection = DriverManager.getConnection(url, "root", "P@ssw0rd");
            //INSERT INTO table_name (column1, column2, column3, ...)
            //VALUES (value1, value2, value3, ...);
            PreparedStatement myStatement = myConnection.prepareStatement("UPDATE Food SET food_name = ? WHERE id = ? ");
            myStatement.setString(1, foodName);
            myStatement.setInt(2 , id);
            myStatement.execute();

        } catch (SQLException e) {
            while (e != null) {
                System.out.println(e);
                e = e.getNextException();
            }
        }
    }
    public List<Tiger> findAllFoodByAnimalId(int animalId){
        String url = "jdbc:mysql://localhost:3306/zoo"; //STEP2
        try (Connection myConnection = DriverManager.getConnection(url, "root", "P@ssw0rd");) {
            PreparedStatement myStatement = myConnection.prepareStatement("select * from Tiger where id = ?");//Remember always use a parameterized (?) query, if you need to add values in your query!
            myStatement.setInt(1, animalId);
            ResultSet myResultSet = myStatement.executeQuery();
            List<Tiger> tigerList = new ArrayList<>(); //
            while (myResultSet.next()) { //
                int id = myResultSet.getInt("id");
                String name = myResultSet.getString("name");
                tigerList.add(new Tiger(name, id));
            }
            return tigerList;

        } catch (SQLException e) {
            while (e != null)  {
                System.out.println(e);
                e = e.getNextException();
            }
            return null; // the getTigersFromDb() method has a return type List<Tiger> so it's required that this returns something
        }
    }
}





    //TODO: add the method findAllFoodByAnimalId(int animalId) which returns a List of Food
    //HINT: select * from Food where animalId = ?


